import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * In the game class, the setting here represents life and moving through it. In this 
 * class there are objects we need to avoid as well as objects we need to "pick up."
 * 
 * @author David Dietz
 * 
 */
public class Game extends World
{
    private int playerType;
    private int score;
    private int time;
    /**
     * Constructor: Set up the staring objects.
     */
    public Game(int playerType)
    {    
        super(780, 360, 1); 
        //setPaintOrder(Border.class);
        if ( playerType == 1 )
        {
            score = 20;
        }
        
        if ( playerType == 2 )
        {
            score = 80;
        }
        
        time = 2000;
        this.playerType = playerType;

        prepare();
        
        showScore();
        showTime();
    } // end method Game

    public void actPlayer1()
    {
        if (Greenfoot.getRandomNumber(100) < 2)
        {
            addObject(new Mask(), 779, Greenfoot.getRandomNumber(360));
        }
    
        if (Greenfoot.getRandomNumber(200) < 2)
        {
            addObject(new Clouds(), 779, 0);
        }
        
        // if statements for obstacles
        if (Greenfoot.getRandomNumber(100) < 1)
        {
            addObject(new Obstacle2(), 779, Greenfoot.getRandomNumber(360));
        }
        
        if (Greenfoot.getRandomNumber(100) < 2)
        {
            addObject(new Obstacle(), 779, Greenfoot.getRandomNumber(360));
        }
        
        if (Greenfoot.getRandomNumber(100) < 1)
        {
            addObject(new Obstacle3(), 779, Greenfoot.getRandomNumber(360));
        }
    } // end method actPlayer1
    
    
    public void actPlayer2()
    {
        if (Greenfoot.getRandomNumber(100) < 3)
        {
            addObject(new Mask(), 779, Greenfoot.getRandomNumber(360));
        }
        
        if (Greenfoot.getRandomNumber(200) < 2)
        {
            addObject(new Clouds(), 779, 0);
        }
        
        // if statements for obstacles
        if (Greenfoot.getRandomNumber(100) < 1)
        {
            addObject(new Obstacle2(), 779, Greenfoot.getRandomNumber(360));
        }
  
        if (Greenfoot.getRandomNumber(100) < 1)
        {
            addObject(new Obstacle(), 779, Greenfoot.getRandomNumber(360));
        }
        
        if (Greenfoot.getRandomNumber(100) < 1)
        {
            addObject(new Obstacle3(), 779, Greenfoot.getRandomNumber(360));
        }
       
    } // end method actPlayer2
    
    
    /**
     * Create new floating objects at irregular intervals.
     */
    public void act()
    {
       if (playerType == 1)
       {
           // do stuff specific to player 1
           actPlayer1();
       }
       else if (playerType == 2)
       {
           // do stuff specific to player 2
           actPlayer2();
       } // end elseif
       
        countTime();
    } // end method act
    
    
    
    /**
     * Add some points to our current score. (May be negative.)
     * If the score falls below 0, game's up.
     */
    public void addScore(int points)
    {
        score = score + points;
        showScore();
        if (score < 1 || score > 99) 
        {
            Greenfoot.playSound("game-over.wav");
            Greenfoot.setWorld( new GameOver() );
            Greenfoot.stop();
        }
    } // end method addScore
    
    /**
     * Show our current score on screen.
     */
    private void showScore()
    {
        showText("Ego Level: " + score, 80, 25);
    } // end method showeScore

    /**
     * Count down the game time and display it. Stop the game 
     * with a winning message when time is up.
     */
    private void countTime()
    {
        time--;
        showTime();
        if (time == 0)
        {
            showEndMessage();
            Greenfoot.stop();
        }
    } // end method countTime

    /**
     * Show the remaining game time on screen.
     */
    private void showTime()
    {
        showText("Time: " + time, 700, 25);
    } // end method showTime
    
    /**
     * Show the end-of-game message on screen.
     */
    private void showEndMessage()
    {
        Greenfoot.playSound("win.wav");
        showText("Time is up - you win!", 390, 150);
        showText("Your Ego Level: " + score, 390, 170);
    } // end method showEndMessage 
    
    /**
     * Prepare the world for the start of the program. In this case: Create
     * a white blood cell and the lining at the edge of the blood stream.
     */
    private void prepare()
    {
        Player1 player1 = new Player1( playerType );
        addObject(player1, 128, 179);
       
        Clouds clouds = new Clouds();
        addObject(clouds, 390, 10);
        
    } // end method prepare
} // end class Game
