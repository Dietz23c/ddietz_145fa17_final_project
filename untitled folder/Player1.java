import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is an obstacle, the purpose of the obstacle is to collect masks to boost 
 * ego level
 * 
 * @author David Dietz
 */
public class Player1 extends Actor
{
    private int playerType;
    
    /**
     * Act: move up and down when cursor keys are pressed and check if player touches 
     * an obstacle
     */
    public void act() 
    {
        checkKeyPress();
        checkCollision();
    } // end method act
    
    public Player1( int playerType )
    {
        this.playerType = playerType;
        setImage( new GreenfootImage("player" + playerType + ".png") );
    } // end method Player1
    
    /**
     * Check whether a keyboard key has been pressed and react if it has.
     */
    private void checkKeyPress()
    {
        if (Greenfoot.isKeyDown("up")) 
        {
            setLocation(getX(), getY()-4);
        } // end if 
        
        if (Greenfoot.isKeyDown("down")) 
        {
            setLocation(getX(), getY()+4);
        } // end if 
        
        if (Greenfoot.isKeyDown("right")) 
        {
            setLocation(getX()+4, getY());
        } // end if
        
        if (Greenfoot.isKeyDown("left")) 
        {
            setLocation(getX()-4, getY());
        } // end if 
    } // end method checkKeyPress
    
    /**
     * Check whether user is touching an obstacle or mask. Removes masks.
     */
    private void checkCollision()
    {
        if (isTouching(Mask.class)) 
        {
            Greenfoot.playSound("mask.wav");
            removeTouching(Mask.class);
            Game game = (Game)getWorld();
            game.addScore(15);
        } // end if

        if (isTouching(Obstacle.class)) 
        {
           Greenfoot.playSound("enemyHit.wav");
           removeTouching(Obstacle.class);
           Game game = (Game)getWorld();
           game.addScore(-15);    
        } // end if
        
        if (isTouching(Obstacle2.class)) 
        {
           Greenfoot.playSound("enemyHit.wav");
           removeTouching(Obstacle2.class);
           Game game = (Game)getWorld();
           game.addScore(-15);    
        } // end if
        
        if (isTouching(Obstacle3.class)) 
        {
           Greenfoot.playSound("enemyHit.wav");
           removeTouching(Obstacle3.class);
           Game game = (Game)getWorld();
           game.addScore(-15);    
        } // end if
    } // end method chekcCollision
} // end class Player1
