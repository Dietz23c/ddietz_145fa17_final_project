import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class GameOver here.
 * 
 * @author (David Dietz) 
 */
public class GameOver extends World
{

    /**
     * Sets gameOverTitle on the screen along with background image
     * 
     */
    public GameOver()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        gameOverTitle gameovertitle = new gameOverTitle();
        addObject(gameovertitle, 300, 200);
    }
}
