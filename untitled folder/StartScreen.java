import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class StartScreen here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class StartScreen extends World
{

    /**
     * Constructor for objects of class StartScreen.
     * 
     */
    public StartScreen()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        prepare();
        
        
    } // end method StartScreen
    private void prepare()
    {
        
        
        Player1Button player1Button = new Player1Button();
        addObject(player1Button, 180, 300);
        
        Player2Button player2Button = new Player2Button();
        addObject(player2Button, 400, 300);
       
        
        
    } // end method prepare
    
    
    
} // end class StartScreen
