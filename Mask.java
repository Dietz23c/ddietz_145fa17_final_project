import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * User wants to collect masks to boost ego level must must be cautious to not 
 * collect too many, lest the game ends 
 * 
 * @author (David Dietz)
 */
public class Mask extends Actor
{
    private int speed;

    /**
     * Constructor: Initialise the speed of the masks to a somewhat random value.
     */
    public Mask()
    {
        speed = Greenfoot.getRandomNumber(3) + 1;
    }// end method Mask
    
    /**
     * Move along the Game.
     */
    public void act() 
    {
        setLocation(getX()-speed, getY());
        
        
        if (getX() == 0) 
        {
            Game game = (Game)getWorld();
            //game.addScore(-15);
            game.removeObject(this);
        } // end if
    } // end method act 
} // end class Mask
