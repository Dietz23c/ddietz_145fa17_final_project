import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * An Obstacle travels through the Game. This object will bring down the players "ego level"
 * 
 * @author (David Dietz) 
 */
public class Obstacle3 extends Actor
{
    /**
     * Move along the screen 
     */
    public void act() 
    {
        setLocation(getX()-4, getY());
        
        
        if (getX() == 0) 
        {
            getWorld().removeObject(this);
        } // end if 
    } // end method act
} // end class Obstacle3
