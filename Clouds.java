import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The clouds are objects at the top of the world to simulate movement 
 * 
 * @author (David Dietz)
 */
public class Clouds extends Actor
{
    /**
     * Move the Clouds along at regular speed.
     */
    public void act() 
    {
        setLocation(getX()-1, getY());
        
        if (getX() == 0) 
        {
            getWorld().removeObject(this);
        } // end if 
    } // end act method 
} // end class Clouds
