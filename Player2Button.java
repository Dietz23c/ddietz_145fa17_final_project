import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This button, once clicked, will spawn a world with Player2Button conditions
 * 
 * @author (David Dietz) 
 */
public class Player2Button extends Actor
{
     /**
     * Initiates the world with conditions to the player that is clicked 
     */
    public void act() 
    {
        click();
    } // end method act 
    
    /**
     * Select Character screen pops up when this is clicked
     */
   
    private void click()
    {
        if (Greenfoot.mouseClicked(this))
        {
            Greenfoot.setWorld( new Game( 2 ) );
        } // end if 
    } // end method click
} // end class Player2Button
